VALID_HTML_TAGS = ['p', 'strong', 'br', 'em', 'span', 'ul', 'li', 'ol',
                   'strike', 'strong', 'div', 'image', 'a', 'code', 'h1',
                   'h2', 'h3', 'h4', 'h5', 'h6', 'center']

VALID_HTML_ATTRIBUTES = {'*': ['class', 'id', 'style', 'title'],
                         'img': ['src'],
                         'a': ['href']}

SECTION_DICT = {'show_tech': True, 'show_add_exp': True, 'show_lang': True, 'show_certi': True}
