import json


def delete_record():
    record_id = request.vars.id
    resume_id = request.vars.resume_id
    table = request.vars.table

    if table == 'section':
        section_record = db(db.section.id == record_id).select().first()
    db(db[table].id == record_id).delete()
    db.commit()

    if table == 'education':
        records = db(db.education.resume == resume_id).select()

        if not records:
            row = db(db.resume.id == resume_id).update(show_education=False)
    elif table == 'employment':
        records = db(db.employment.resume == resume_id).select()

        if not records:
            row = db(db.resume.id == resume_id).update(show_employment=False)
    elif table == 'section':
        records = db(db.resume.id == resume_id).select()
        section_list_in_json = records[0]['show_section']
        section_list = json.loads(section_list_in_json)

        if section_record:
            if section_record['section_name'] == 'Technical Experience':
                section_list['show_tech'] = False
            elif section_record['section_name'] == 'Additional Experience and Awards':
                section_list['show_add_exp'] = False
            elif section_record['section_name'] == 'Languages and Technologies':
                section_list['show_lang'] = False
            elif section_record['section_name'] == 'Certification':
                section_list['show_certi'] = False
        else:
            if record_id == '-1':
                section_list['show_tech'] = False
            elif record_id == '-2':
                section_list['show_add_exp'] = False
            elif record_id == '-3':
                section_list['show_lang'] = False
            elif record_id == '-4':
                section_list['show_certi'] = False

        row = db(db.resume.id == resume_id).update(show_section=json.dumps(section_list))
