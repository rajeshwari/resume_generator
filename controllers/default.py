# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

# -------------------------------------------------------------------------
# This is a sample controller
# - index is the default action of any application
# - user is required for authentication and authorization
# - download is for downloading files uploaded in the db (does streaming)
# -------------------------------------------------------------------------
import pdfkit
import os
import bleach
from constants import VALID_HTML_TAGS, VALID_HTML_ATTRIBUTES, SECTION_DICT
import json


def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """

    return dict()


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


def create():
    show_education = session.get('show_education', True)
    show_employment = session.get('show_employment', True)
    show_tech_exp = session.get('show_tech_exp', True)
    show_exp_and_awards = session.get('show_exp_and_awards', True)
    show_lang_and_tech = session.get('show_lang_and_tech', True)

    create_index = request.args[0]

    personal_info = db(db.resume.id == create_index).select(db.resume.ALL).first()

    employment = db(db.employment.resume == create_index).select()

    education = db(db.education.resume == create_index).select()

    section = db(db.section.resume == create_index).select()

    show_education = personal_info['show_education']
    show_employment = personal_info['show_employment']

    section_list = section.as_list()

    show_section = json.loads(personal_info['show_section'])
    if not personal_info.name:
        personal_info = {'name': 'Richard B. Trump', 'address': '2601,Oxford street,NYC',
                         'mobile': '9865234174', 'email': 'rtrump@gmail.com'}

    if not employment:
        employment = [{'id': -1, 'post': 'Software Engineer, Intern',
                      'company': 'Apple Computer', 'start_date': '20/11/2013',
                      'end_date': '02/01/2014',
                      'work_info': 'Reduced time to render the user\'s buddy list by 75%,Implemented iChat integration,Redesigned chat file format and implemented backwards compatibility for search'}]

    if not education:
        education = [{'id': -1, 'degree': 'B.S.E. in Computer Science',
                      'university': 'University of Pennsylvania, Philadelphia',
                      'year': '2010 to 2014',
                      'course_info': 'Undergraduate Coursework:Operating Systems; Databases; Algorithms; Programming Languages;Comp. Architecture; Engineering Entrepreneurship; Calculus III'}]

    new_section_list = []

    if show_section['show_tech']:
        new_section_list.append({'id': -1, 'section_name': 'Technical Experience',
                                                    'left_heading': 'Projects', 'right_heading': '', 'centre_heading': '',
                                                    'description': 'Multi-User Drawing Tool (2004) - Electronic classroom where multiple users can view and simultan'})
    if show_section['show_add_exp']:
        new_section_list.append({'id': -2, 'section_name': 'Additional Experience and Awards',
                                                    'left_heading': '', 'right_heading': '', 'centre_heading': '',
                                                    'description': 'Blogging: Blogger at ImageDashboard.com(photoshop blog) and vichare GauravVichare.com'})
    if show_section['show_lang']:
        new_section_list.append({'id': -3, 'section_name': 'Languages and Technologies',
                                                    'left_heading': '', 'right_heading': '', 'centre_heading': '',
                                                    'description': 'C++; C; Java; Objective-C; C#;Visual Studio; Microsoft SQL Server; Eclipse; XCode'})
    if show_section['show_certi']:
        new_section_list.append({'id': -4, 'section_name': 'Certification',
                                                    'left_heading': '', 'right_heading': '', 'centre_heading': '',
                                                    'description': 'Best student award'})

    for row in section_list:
        index = get_index(row['section_name'], new_section_list)

        if index > -1:
            new_section_list[index] = row
        else:
            new_section_list.append(row)
 

    return dict(show_education=show_education, show_employment=show_employment,
                show_tech_exp=show_tech_exp, show_exp_and_awards=show_exp_and_awards,
                show_lang_and_tech=show_lang_and_tech, personal_info=personal_info,
                employment=employment, education=education, section_list=new_section_list)


def get_index(section_name, new_section_list):

    counter = 0
    for section in new_section_list:
        if section_name == section['section_name']:
            return counter
        counter = counter + 1 
    return -1


def edit_name_box():

    edit_index = request.args(0, cast=int)
    # record = db.resume(request.args(0, cast=int)) or redirect(URL('index'))
    record = db(db.resume.id == edit_index).select().first()

    form = SQLFORM(db.resume, record, showid=False).process()

    if form.accepted:

        response.flash = "Record modified"

        redirect(URL('default', 'create', args=edit_index))

    return dict(form=form)


def edit_employment_box():

    employment_id = request.args(0, cast=int)

    record = db(db.employment.id == employment_id).select().first()
    db.employment.resume.default = request.vars.resume_id
    edit_id = request.vars.resume_id
    form = SQLFORM(db.employment, record, buttons=[TAG.button('Back', _type="button", _onClick="parent.location='%s'" % URL('default', 'create', args=edit_id)),
                                                   TAG.button('Submit', _type="submit")])

    if form.process(onvalidation=employ_validation).accepted:

        response.flash = "Record modified"

        redirect(URL('default', 'create', args=edit_id))

    return dict(form=form)


def edit_education_box():

    education_id = request.args(0, cast=int)

    record = db(db.education.id == education_id).select().first()
    db.education.resume.default = request.vars.resume_id
    edit_id = request.vars.resume_id

    form = SQLFORM(db.education, record, buttons=[TAG.button('Back', _type="button", _onClick="parent.location='%s'" % URL('default', 'create', args=edit_id)),
                                                  TAG.button('Submit', _type="submit")])

    if form.process(onvalidation=edu_validation).accepted:

        response.flash = "Record modified"

        redirect(URL('default', 'create', args=edit_id))

    return dict(form=form)


def edu_validation(form):
    edu_info = form.vars.course_info
    edu_info_updated = bleach.clean(edu_info, tags=VALID_HTML_TAGS, attributes=VALID_HTML_ATTRIBUTES)
    form.vars.course_info = edu_info_updated


def employ_validation(form):
    employ_info = form.vars.work_info
    employ_info_updated = bleach.clean(employ_info, tags=VALID_HTML_TAGS, attributes=VALID_HTML_ATTRIBUTES)
    form.vars.work_info = employ_info_updated


def edit_section_box():

    section_id = request.args(0, cast=int)

    record = db(db.section.id == section_id).select().first()
    db.section.resume.default = request.vars.resume_id
    edit_id = request.vars.resume_id

    db.section.section_name.default = request.vars.section_name

    db.section.left_heading.show_if = db.section.section_name.belongs(('Other', 'Technical Experience'))
    db.section.centre_heading.show_if = (db.section.section_name == 'Other')
    db.section.right_heading.show_if = (db.section.section_name == 'Other')

    form = SQLFORM(db.section, record, buttons=[TAG.button('Back', _type="button", _onClick="parent.location='%s'" % URL('default', 'create', args=edit_id)),
                                                TAG.button('Submit', _type="submit")])

    if form.process(onvalidation=my_form_processing).accepted:

        response.flash = "Record modified"

        redirect(URL('default', 'create', args=edit_id))

    return dict(form=form)


def my_form_processing(form):
    section_name = form.vars.section_name
    left_heading = form.vars.left_heading
    right_heading = form.vars.right_heading
    centre_heading = form.vars.centre_heading
    description = form.vars.description

    if description == '':
        form.errors.description = 'Cannot Be Empty!'

    if section_name == 'Technical Experience' and left_heading == '':
        form.errors.left_heading = 'Cannot Be Empty!'

    description_updated = bleach.clean(description, tags=VALID_HTML_TAGS, attributes=VALID_HTML_ATTRIBUTES)
    form.vars.description = description_updated

    # elif section_name == 'Other':
    #     if left_heading == '':
    #        form.errors.left_heading = 'Cannot Be Empty!' 
    #     if right_heading == '':
    #         form.errors.right_heading = 'Cannot Be Empty!'
    #     if centre_heading == '':
    #         form.errors.centre_heading = 'cannot be empty!'


def download_resume():
    section_id = request.args(0, cast=int)
    url = URL('default', 'create', args=section_id, scheme=True, host=True)
    file_path = os.path.join('applications', 'resume_generator', 'uploads', 'out.pdf')
    pdfkit.from_url(url, file_path)
    return response.stream(open(file_path, 'rb'), chunk_size=10**6, attachment=True)


def create_resume():
    record_id = db.resume.insert(name='')
    db.commit()
    redirect(URL('default', 'create', args=record_id))
